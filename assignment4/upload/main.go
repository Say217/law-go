package main

import (
	"io"
	"log"
	"net/http"
	"os"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func homeEndPoint(c *gin.Context) {
	c.HTML(http.StatusOK, "upload.html", nil)
}

func uploadServiceEndPoint(c *gin.Context) {
	c.Request.ParseMultipartForm(10 << 20)
	file, handler, err := c.Request.FormFile("file")
	if err != nil {
		log.Println(err)
		c.JSON(422, gin.H{
			"upload_status": "upload failed",
		})
		return
	}
	defer file.Close()

	dst, err := os.Create("../temp/" + handler.Filename)
	if err != nil {
		log.Println(err)
		c.JSON(409, gin.H{
			"upload_status": "cannot create file",
		})
		return
	}
	defer dst.Close()

	_, err = io.Copy(dst, file)
	if err != nil {
		log.Println(err)
		c.JSON(409, gin.H{
			"upload_status": "cannot write file",
		})
		return
	}

	c.HTML(http.StatusOK, "download.html", gin.H{
		"download_url": "/download/" + handler.Filename,
	})

}

func main() {
	if _, err := os.Stat("../temp/"); os.IsNotExist(err) {
		os.Mkdir("../temp/", 0700)
	}

	r := gin.Default()
	r.Use(cors.Default())

	r.LoadHTMLGlob("templates/*")

	r.GET("/upload", homeEndPoint)
	r.POST("/upload", uploadServiceEndPoint)
	r.Run(":8001")
}

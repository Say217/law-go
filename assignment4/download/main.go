package main

import (
	"log"
	"path/filepath"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func downloadEndPoint(c *gin.Context) {
	fileName := c.Param("filename")
	log.Println(fileName)
	downloadPath := filepath.Join("../temp/", fileName)
	c.Header("Content-Description", "File Transfer")
	c.Header("Content-Transfer-Encoding", "binary")
	c.Header("Content-Disposition", "attachment; filename="+fileName)
	c.Header("Content-Type", "application/octet-stream")
	c.File(downloadPath)
}

func main() {
	r := gin.Default()
	r.Use(cors.Default())

	r.GET("/download/:filename", downloadEndPoint)
	r.Run(":8002")
}

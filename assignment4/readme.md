## Assignment 4

### Upload Service dan Download Service

Upload service dan download service bisa dijalankan dengan menjalankan kode main.go yang ada di dalam folder upload dan download. Sebelum kode dijalan, perlu dipastikan modul yang diperlukan sudah ada. Maka dari itu kira-kira cara untuk menjalankan service ini adalah dengan menjalankan command berikut:

```sh
cd upload
go mod tidy
go run main.go
cd ../download/
go mod tidy
go run main.go
```

Upload service berjalan pada port 8001 sedangkan download service berjalan pada port 8002.

### NGINX
Pada Linux(raspberrypi OS) konfigurasi nginx bisa digunakan dengan mengcopy file assg4.conf ke /etc/nginx/conf.d/
Pastikan tidak ada default.conf didalamnya atau replace defaul.conf dengan isi dari assg4.conf.
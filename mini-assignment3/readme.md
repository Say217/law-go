## Mini Project 3

### Web Server dan Download Agent

Upload service dan download service bisa dijalankan dengan menjalankan kode web-service.go yang ada di dalam folder web_service dan download_agent.go yang ada di dalam folder download_agent. Sebelum kode dijalan, perlu dipastikan modul yang diperlukan sudah ada. Maka dari itu kira-kira cara untuk menjalankan service ini adalah dengan menjalankan command berikut (asumsi rabbitmq sudah dijalankan):

```sh
cd web_server
go mod tidy
go run web_server.go
cd ../download_agent/
go mod tidy
go run download_agent.go
```

### GRPC
Upload service berjalan pada port 80 sedangkan download agent pada port 9000. Keduanya berkomunikasi dengan protokol grpc dengan web_server sebagai client dan download_agent sebagai server. Pada mini project ini saya tidak menggunakan command wget secara langsung untuk mendownload file dari url yang diberikan, melainkan dengan package "github.com/cavaliercoder/grab" yang juga sudah menyediakan fitur monitoring progress download. Saya menjalankan proses download dengan package tersebut secara asynchronus dengan menggunakan go routine (file miniproject3.go line 112).

### RabbitMQ
RabbitMQ berguna untuk mengupdate browser mengenai progress pendownloadan file ke sharedstorage. ketika download sudah selesai, browser akan diberikan url untuk mendownload file tersebut dari web server.
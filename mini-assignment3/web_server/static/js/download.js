document.getElementById("content").innerHTML = '<div class="progress">'+
'<div class="progress-bar" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>'+
'</div>';
let ws_stomp_display = new WebSocket("ws://127.0.0.1:15674/ws");
let client_display = Stomp.over(ws_stomp_display);
let mq_queue_display = "/exchange/download/"+_uniqID;

let on_connect_display = () => {
    console.log("connected");
    client_display.subscribe(mq_queue_display, on_message_display);
};

let on_error_display = () => {
    console.log("error");
};

let on_message_display = (m) => {
    console.log("message received");
    console.log(m.body)
    splittedBody = m.body.split(" ");
    
    if(splittedBody[0] == "X"){
        document.getElementById("content").innerHTML = '<div class="progress">' +
        '<div class="progress-bar" role="progressbar" style="width: '+ splittedBody[1] +
        '%" aria-valuenow="'+ splittedBody[1] +
        '" aria-valuemin="0" aria-valuemax="100"></div>' +
        '</div>';
    }
    else {
    document.getElementById("content").innerHTML = '<form  action="/download/'+ splittedBody[1] +'" method="get" enctype="multipart/form-data">' +
    '<div class="d-flex justify-content-center">' +
    '<button type="submit" class="btn btn-primary">Download</button>' +
    '</div>' +
    '</form>'
    }
};

client_display.connect(
    "guest",
    "guest",
    on_connect_display,
    on_error_display,
    "/"
);
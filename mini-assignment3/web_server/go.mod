module miniassg3/web_server

go 1.16

replace miniassg3/download_agent => ../download_agent

require (
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.7.2
	google.golang.org/grpc v1.38.0
	miniassg3/download_agent v0.0.0-00010101000000-000000000000
)

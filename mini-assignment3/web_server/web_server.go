package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"path/filepath"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"

	"miniassg3/download_agent/miniproject3"

	"google.golang.org/grpc"
)

func downloadPageEndPoint(c *gin.Context) {
	c.HTML(http.StatusOK, "download_form.html", nil)
}

func startDownloadEndPoint(c *gin.Context) {
	// init grpc connection with download agent
	conn, err := grpc.Dial(":9000", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not connect: %s", err)
	}
	defer conn.Close()
	client := miniproject3.NewMiniProjectServiceClient(conn)

	url := c.Request.FormValue("url")

	request := miniproject3.DownloadRequest{
		Url: url,
	}

	response, err := client.Download(context.Background(), &request)
	if err != nil {
		log.Fatalf("error when calling Download %s", err)
	}
	log.Printf("Response from Server: %s and %s", response.Url, response.UniqId)
	c.HTML(http.StatusOK, "download_progress.html", gin.H{
		"uniqId": response.UniqId,
	})
}

func downloadEndPoint(c *gin.Context) {
	fileName := c.Param("filename")
	log.Println(fileName)
	downloadPath := filepath.Join("../temp/", fileName)
	c.Header("Content-Description", "File Transfer")
	c.Header("Content-Transfer-Encoding", "binary")
	c.Header("Content-Disposition", "attachment; filename="+fileName)
	c.Header("Content-Type", "application/octet-stream")
	c.File(downloadPath)
}

func main() {
	if _, err := os.Stat("../temp/"); os.IsNotExist(err) {
		os.Mkdir("../temp/", 0700)
	}

	// init gin router
	r := gin.Default()
	r.Use(cors.Default())

	r.Static("/static", "./static")
	r.LoadHTMLGlob("templates/*")

	r.GET("/", downloadPageEndPoint)
	r.POST("/", startDownloadEndPoint)
	r.GET("/download/:filename", downloadEndPoint)
	r.Run(":80")
}

package main

import (
	"log"
	"net"

	"miniassg3/download_agent/miniproject3"

	"google.golang.org/grpc"
)

func main() {
	miniproject3.Token = make(map[string]string)
	lis, err := net.Listen("tcp", ":9000")
	if err != nil {
		log.Fatalf("Failed to listen on port 9000: %v", err)
	}
	s := miniproject3.Server{}
	grpcServer := grpc.NewServer()

	miniproject3.RegisterMiniProjectServiceServer(grpcServer, &s)

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("Failed to listen on port 9000: %v", err)
	}
}

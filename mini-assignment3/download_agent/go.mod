module miniassg3/download_agent

go 1.16

require (
	github.com/cavaliercoder/grab v2.0.0+incompatible
	github.com/streadway/amqp v1.0.0
	golang.org/x/net v0.0.0-20210525063256-abc453219eb5
	google.golang.org/grpc v1.38.0
	google.golang.org/protobuf v1.26.0
)

package miniproject3

import (
	"fmt"
	"log"
	"math/rand"
	"os"
	"time"

	"github.com/cavaliercoder/grab"
	"github.com/streadway/amqp"
	"golang.org/x/net/context"
)

var (
	letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
	Token   map[string]string
)

type Server struct {
}

func publishMQ(status string, routingKey string) {
	conn, err := amqp.Dial("amqp://guest:guest@127.0.0.1:5672/")
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	err = ch.ExchangeDeclare(
		"download", // name
		"direct",   // type
		true,       // durable
		false,      // auto-deleted
		false,      // internal
		false,      // no-wait
		nil,        // arguments
	)
	failOnError(err, "Failed to declare an exchange")

	err = ch.Publish(
		"download", // exchange
		routingKey, // routing key
		false,      // mandatory
		false,      // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(status),
		})
	failOnError(err, "Failed to publish a message")

	log.Printf(" [x] Sent %s", status)
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func downloadFromURL(url string, key string) {
	// create client
	client := grab.NewClient()
	req, _ := grab.NewRequest("../temp/", url)

	// start download
	log.Printf("Downloading %v...\n", req.URL())
	resp := client.Do(req)
	log.Printf("  %v\n", resp.HTTPResponse.Status)

	// start UI loop
	t := time.NewTicker(time.Second)
	defer t.Stop()

Loop:
	for {

		select {
		case <-t.C:
			log.Printf("  transferred %v / %v bytes (%f%%)\n",
				resp.BytesComplete(),
				resp.Size,
				100*resp.Progress())
			progress := fmt.Sprintf("%f", resp.Progress()*100)
			publishMQ("X "+progress, key)

		case <-resp.Done:
			// download is complete
			filename := resp.Filename[8:]
			publishMQ("O "+filename, key)
			removeKey(key)
			break Loop
		}
	}

	// check for errors
	if err := resp.Err(); err != nil {
		fmt.Fprintf(os.Stderr, "Download failed: %v\n", err)
		os.Exit(1)
	}

	log.Printf("Download saved to ./%v \n", resp.Filename)

}

func (s *Server) Download(ctx context.Context, request *DownloadRequest) (*DownloadResponse, error) {
	newKey := generateKey(32)
	Token[newKey] = newKey

	go downloadFromURL(request.Url, newKey)

	log.Printf("Received message body from client: %s", request.Url)
	return &DownloadResponse{Url: request.Url, UniqId: newKey}, nil
}

func generateKey(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	key := string(b)
	if usableKey(key) {
		return key
	} else {
		return generateKey(n)
	}
}

func usableKey(key string) bool {
	if _, exist := Token[key]; exist {
		return false
	}
	return true
}

func removeKey(key string) {
	delete(Token, key)
}

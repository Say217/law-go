package main

import (
	"fmt"
	"log"
	"os"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

type Mahasiswa struct {
	gorm.Model
	Nama string
	NPM  string `gorm:"unique"`
}

func initialMigration() {
	db, err := gorm.Open("sqlite3", "../db/assg5.db")
	if err != nil {
		fmt.Println(err.Error())
		panic("failed to connect database")
	}
	defer db.Close()

	// Migrate the schema
	db.AutoMigrate(&Mahasiswa{})
}

func newMahasiswaEndPoint(ctx *gin.Context) {
	db, err := gorm.Open("sqlite3", "../db/assg5.db")
	if err != nil {
		fmt.Println(err.Error())
		panic("failed to connect database")
	}
	defer db.Close()

	nama := ctx.Request.FormValue("Nama")
	npm := ctx.Request.FormValue("NPM")

	if err := db.Create(&Mahasiswa{Nama: nama, NPM: npm}).Error; err != nil {
		updateMahasiswa(ctx)
		return
	}

	log.Printf("Mahasiswa %s created successfully", nama)

	ctx.JSON(201, gin.H{
		"status": "OK",
	})
}

func updateMahasiswa(ctx *gin.Context) {
	db, err := gorm.Open("sqlite3", "../db/assg5.db")
	if err != nil {
		fmt.Println(err.Error())
		panic("failed to connect database")
	}
	defer db.Close()

	nama := ctx.Request.FormValue("Nama")
	npm := ctx.Request.FormValue("NPM")

	if err := db.Model(&Mahasiswa{}).Where("NPM = ?", npm).Update("Nama", nama).Error; err != nil {
		ctx.JSON(409, gin.H{
			"status": "Failed to update entry",
		})
		return
	}

	log.Printf("Mahasiswa %s updated successfully", nama)

	ctx.JSON(201, gin.H{
		"status": "OK",
	})
}

func main() {
	if _, err := os.Stat("../db/"); os.IsNotExist(err) {
		os.Mkdir("../db/", 0700)
	}

	initialMigration()

	r := gin.Default()
	r.Use(cors.Default())

	r.POST("/", newMahasiswaEndPoint)
	r.Run(":8000")
}

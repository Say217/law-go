package main

import (
	"fmt"
	"log"
	"os"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

type Mahasiswa struct {
	gorm.Model
	Nama string
	NPM  string `gorm:"unique"`
}

func initialMigration() {
	db, err := gorm.Open("sqlite3", "../db/assg5.db")
	if err != nil {
		fmt.Println(err.Error())
		panic("failed to connect database")
	}
	defer db.Close()

	// Migrate the schema
	db.AutoMigrate(&Mahasiswa{})
}

func getMahasiswaEndPoint(ctx *gin.Context) {
	db, err := gorm.Open("sqlite3", "../db/assg5.db")
	if err != nil {
		fmt.Println(err.Error())
		panic("failed to connect database")
	}
	defer db.Close()

	npm := ctx.Param("NPM")

	var mahasiswa Mahasiswa

	db.Where("npm = ?", npm).Find(&mahasiswa)

	log.Printf("Mahasiswa %s  fetched successfully", mahasiswa.Nama)

	ctx.JSON(200, gin.H{
		"status": "OK",
		"npm":    mahasiswa.NPM,
		"nama":   mahasiswa.Nama,
	})
}

func main() {
	if _, err := os.Stat("../db/"); os.IsNotExist(err) {
		os.Mkdir("../db/", 0700)
	}

	initialMigration()

	r := gin.Default()
	r.Use(cors.Default())

	r.GET("/read/:NPM", getMahasiswaEndPoint)
	r.GET("/read/:NPM/:_", getMahasiswaEndPoint)
	r.Run(":8001")
}

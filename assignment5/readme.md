# Assignment 5
## Port dan Endpoint
update service  : berjalan pada port 8000 dengan endpoint /

read service    : berjalan pada port 8001 dengan endpoint /read/:NPM untuk endpoint yang tidak menggunakan cache dan /read/:NPM/:_ untuk endpoint yang menggunakan cache

## Konfigurasi NGINX
NGINX dikonfigurasi melakukan reverse proxy dan caching. NGINX akan melakukan reverse proxy pada /update/ yang akan diarahkan ke / pada port 8000 dan /read/:NPM yang akan diarahkan ke /read/:NPM pada port 8001, dan yang terakhir /read/:NPM/:trx_id yang akan diarahkan ke /read/:NPM/:trx_id pada port 8001 juga dan pada endpoint ini juga dilakukan caching.

## Cached vs No Cache
Berikut adalah hasil stress test yang dilakukan pada kasus tanpa cache dan dengan cache dengan concurrency 100 user dan total 1000 request:

No Cache
![No Cache](No_Cache.PNG)

Cached
![Cached](Cached.PNG)

Terlihat pada stress test tanpa cache, test memakan waktu sampai 45 ms sedangkan pada stress test dengan cache test hanya memakan waktu 5 ms (Perlu diketahui bahwa test pada kasus cache dilakukan setelah cache disimpan). Dapat dilihat bahwa penggunaan cache sangat mempercepat proses *get request*. 
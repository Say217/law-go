package main

import (
	"fmt"
	"math/rand"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
)

type Session struct {
	AccessToken  string
	RefreshToken string
	UserId       string
	TimeCreated  int64
}

type User struct {
	UserId       string
	FullName     string
	Password     string
	NPM          string
	ClientId     string
	ClientSecret string
}

var (
	users    map[string]User
	sessions map[string]Session
	letters  = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
)

func main() {
	rand.Seed(time.Now().UnixNano())
	users = make(map[string]User)
	sessions = make(map[string]Session)
	userId := "sayid"
	users[userId] = User{UserId: userId, FullName: "Sayid Abyan Rizal Shiddiq", Password: "sayidsayid", NPM: "1706022445", ClientId: "dummyClientID", ClientSecret: "dummyClientSecret"}

	r := gin.Default()
	// r.GET("/", func(c *gin.Context) {
	// 	c.JSON(200, gin.H{
	// 		"message": "Hello World",
	// 	})
	// })
	r.POST("/oauth/token", authLogin)
	r.POST("/oauth/resource", authResource)

	r.Run()
}

func authLogin(c *gin.Context) {
	clientId := c.Request.FormValue("client_id")
	clientSecret := c.Request.FormValue("client_secret")
	if verifyClient(clientId, clientSecret) {
		userId := c.Request.FormValue("username")
		password := c.Request.FormValue("password")
		if verifyLogin(userId, password) {
			accessToken := randSeq(40)
			refreshToken := randSeq(40)
			timeCreated := time.Now().Unix()
			sessions[accessToken] = Session{AccessToken: accessToken, RefreshToken: refreshToken, UserId: userId, TimeCreated: timeCreated}
			c.JSON(200, gin.H{
				"access_token":  accessToken,
				"expires_in":    300,
				"token_type":    "Bearer",
				"scope":         nil,
				"refresh_token": refreshToken,
			})
		} else {
			throwErr(c)
		}
	} else {
		throwErr(c)
	}
}

func authResource(c *gin.Context) {
	bearerToken := c.Request.Header.Get("Authorization")
	token := strings.Fields(bearerToken)[1]
	if verifyToken(token) {
		session := sessions[token]
		user := users[session.UserId]
		c.JSON(200, gin.H{
			"access_token":  token,
			"client_id":     300,
			"user_id":       user.UserId,
			"full_name":     user.FullName,
			"npm":           user.NPM,
			"expires":       nil,
			"refresh_token": session.RefreshToken,
		})
	} else {
		throwErr(c)
	}
}

func verifyLogin(userId string, password string) bool {
	for key, element := range users {
		if element.UserId == userId && element.Password == password {
			fmt.Println(key + " logging in")
			return true
		}
	}
	return false
}
func verifyClient(clientId string, clientSecret string) bool {
	for key, element := range users {
		if element.ClientId == clientId && element.ClientSecret == clientSecret {
			fmt.Println(key + " accessing")
			return true
		}
	}
	return false
}

func verifyToken(token string) bool {
	for key, element := range sessions {
		if key == token {
			if time.Now().Unix()-element.TimeCreated <= 300 {
				fmt.Println(element.UserId + "accessing data")
				return true
			}
		}
	}
	return false
}

func throwErr(c *gin.Context) {
	c.JSON(401, gin.H{
		"error":             "invalid_request",
		"Error_description": "ada kesalahan masbro!",
	})
}

//https://www.calhoun.io/creating-random-strings-in-go/
func randSeq(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

package main

import (
	"bytes"
	"compress/gzip"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"path/filepath"
	"strconv"
	"sync"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/streadway/amqp"
)

func compress(file multipart.File, handler *multipart.FileHeader, routingKey string) {
	buf := bytes.NewBuffer(nil)

	_, err := io.Copy(buf, file)

	if err != nil {
		log.Println(err)
		return
	}
	data := buf.Bytes()

	gBuf := bytes.NewBuffer(nil)
	gWritter := gzip.NewWriter(gBuf)
	gWritter.Name = handler.Filename

	for i := 0; i < 10; i++ {
		slice := data[i*int(handler.Size)/10 : (i+1)*int(handler.Size)/10]
		_, err = gWritter.Write(slice)
		if err != nil {
			log.Println(err)
			return
		}
		status := (i + 1) * 10

		publishMQ(strconv.Itoa(status), routingKey)
	}

	err = gWritter.Close()
	if err != nil {
		log.Fatal(err)
	}

	ioutil.WriteFile("temp/"+handler.Filename+".zip", gBuf.Bytes(), 0777)
}

func publishMQ(status string, routingKey string) {
	conn, err := amqp.Dial("amqp://guest:guest@127.0.0.1:5672/")
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	err = ch.ExchangeDeclare(
		"1706022445", // name
		"direct",     // type
		true,         // durable
		false,        // auto-deleted
		false,        // internal
		false,        // no-wait
		nil,          // arguments
	)
	failOnError(err, "Failed to declare an exchange")

	err = ch.Publish(
		"1706022445", // exchange
		routingKey,   // routing key
		false,        // mandatory
		false,        // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(status),
		})
	failOnError(err, "Failed to publish a message")

	log.Printf(" [x] Sent %s", status)
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func uploadService2EndPoint(c *gin.Context) {
	routingKey := c.Request.Header.Get("X-ROUTING-KEY")

	file, handler, err := c.Request.FormFile("file")
	if err != nil {
		log.Println(err)
		c.JSON(422, gin.H{
			"upload_status": "upload failed",
		})
	}

	var wg sync.WaitGroup

	wg.Add(1)
	go func() {
		wg.Done()
		compress(file, handler, routingKey)
		defer file.Close()
		publishMQ("http://localhost:8001/download-file/"+handler.Filename+".zip", routingKey)
	}()

	c.JSON(200, gin.H{
		"upload_status": "upload success",
	})

	wg.Wait()
}

func downloadEndPoint(c *gin.Context) {
	fileName := c.Param("filename")
	downloadPath := filepath.Join("temp/", fileName)
	c.Header("Content-Description", "File Transfer")
	c.Header("Content-Transfer-Encoding", "binary")
	c.Header("Content-Disposition", "attachment; filename="+fileName)
	c.Header("Content-Type", "application/octet-stream")
	c.File(downloadPath)
}

func main() {
	r := gin.Default()
	r.Use(cors.Default())

	r.POST("/upload-file", uploadService2EndPoint)
	r.GET("/download-file/:filename", downloadEndPoint)
	r.Run(":8001")
}

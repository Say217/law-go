package main

import (

	//	"math/rand"
	"bytes"
	"io"
	"log"
	"math/rand"
	"mime/multipart"
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

type UploadStatus struct {
	UploadStatus string `json:"upload_status"`
}

var (
	letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
	client  = &http.Client{}
)

func generateKey(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func homeEndPoint(c *gin.Context) {
	c.HTML(http.StatusOK, "index.html", nil)
}

func uploadService1EndPoint(c *gin.Context) {
	c.Request.ParseMultipartForm(1024 << 20)
	file, handler, err := c.Request.FormFile("file")
	if err != nil {
		log.Println(err)
		return
	}
	defer file.Close()

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)

	fw, err := writer.CreateFormFile("file", handler.Filename)
	if err != nil {
		log.Println(err)
		return
	}

	_, err = io.Copy(fw, file)
	if err != nil {
		log.Println(err)
		return
	}

	writer.Close()

	request, err := http.NewRequest("POST", "http://127.0.0.1:8001/upload-file", bytes.NewReader(body.Bytes()))
	if err != nil {
		log.Println(err)
		return
	}

	request.Header.Set("Content-Type", writer.FormDataContentType())

	routingKey := generateKey(32)
	request.Header.Set("X-ROUTING-KEY", routingKey)

	response, err := client.Do(request)
	if err != nil {
		log.Println(err)
		return
	}
	defer response.Body.Close()

	c.JSON(200, gin.H{
		"routing_key": routingKey,
	})
}

func main() {
	r := gin.Default()
	r.Use(cors.Default())

	r.LoadHTMLGlob("templates/*")
	r.Static("/static", "./static")

	r.GET("/", homeEndPoint)
	r.POST("/upload-file", uploadService1EndPoint)
	r.GET("/upload-status")
	r.Run(":8000")
}

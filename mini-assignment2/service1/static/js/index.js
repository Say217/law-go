function upload() {
  let formData = new FormData();
  let file = document.getElementById("file");
  formData.append("file", file.files[0]);
  axios.post("http://127.0.0.1:8000/upload-file", formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  }).then(response=>{
    progressConsummer(response.data['routing_key'])
  });
}

function progressConsummer(routingKey) {
  document.getElementById("content").innerHTML ='<div class="progress">'+
  '<div class="progress-bar" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>'+
  '</div>';

  let ws_stomp_display = new WebSocket("ws://127.0.0.1:15674/ws");
  let client_display = Stomp.over(ws_stomp_display);
  let mq_queue_display = "/exchange/1706022445/"+routingKey;

  let on_connect_display = () => {
    console.log("connected");
    client_display.subscribe(mq_queue_display, on_message_display);
  };

  let on_error_display = () => {
    console.log("error");
  };

  let on_message_display = (m) => {
    console.log("message received");
    let body = parseInt(m.body);

    if(!isNaN(body)){
      document.getElementById("content").innerHTML ='<div class="progress">'+
      '<div class="progress-bar" role="progressbar" style="width: '+ m.body +
      '%" aria-valuenow="'+ m.body +
      '" aria-valuemin="0" aria-valuemax="100"></div>'+
      '</div>';
    }
    else {
      window.open(m.body)
    }
  };

  client_display.connect(
    "guest",
    "guest",
    on_connect_display,
    on_error_display,
    "/"
  );
}
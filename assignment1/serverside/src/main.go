package main

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

// Book Struct (Model)
type Book struct {
	ID     string  `json:"id"`
	Isbn   string  `json:"isbn"`
	Title  string  `json:"title"`
	Author *Author `json:"author"`
}

// Author Struct
type Author struct {
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
}

// Init mock books var
var books []Book

// Get All Books
func getBooks(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(books)
}

func main() {
	// Init mux Router
	router := mux.NewRouter()

	origins := handlers.AllowedOrigins([]string{"*"})

	// Append Mock data
	books = append(books, Book{ID: "1", Isbn: "896172", Title: "LAW is Fun", Author: &Author{Firstname: "Sayid", Lastname: "Abyan"}})
	books = append(books, Book{ID: "2", Isbn: "891627", Title: "LAW is Not Fun", Author: &Author{Firstname: "Sayid", Lastname: "Abyan"}})

	// Route Handlers / Endpoints
	router.HandleFunc("/api/books", getBooks).Methods("GET")
	log.Fatal(http.ListenAndServe(":8000", handlers.CORS(origins)(router)))
}

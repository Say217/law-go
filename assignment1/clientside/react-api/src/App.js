    // src/App.js

    import React, {Component} from 'react';
    import Books from './components/books'

    class App extends Component {

      state = {
        books: []
      }

      componentDidMount(){
        fetch("http://localhost:8000/api/books")
        .then(res => res.json())
        .then((data) => {
          console.log(data)
          this.setState({ books: data})
        })
        .catch(console.log)
      }
      render () {
        return (
          // JSX to render goes here...
          <Books books={this.state.books}/>
        );
      }
    }

    export default App;
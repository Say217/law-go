import React from 'react'

const Books = ({ books }) => {
  return (
    <div>
      <center><h1>Book List</h1></center>
      {books.map((book) => (
        <div className="card" key={book.id}>
          <div className="card-body">
            <h5 className="card-title">{book.title}</h5>
            <h6 className="card-subtitle mb-2 text-muted">Author {book.author.firstname} {book.author.lastname}</h6>
            <p className="card-text">ISBN {book.isbn}</p>
          </div>
        </div>
      ))}
    </div>
  )
};

export default Books
package main

import (
	"fmt"
	"log"
	"math/rand"
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"github.com/streadway/amqp"
)

var (
	upgrader = websocket.Upgrader{}
	//mock id, to let client differentiate message sender
	tokens  map[string]bool
	letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
)

func reader(conn *websocket.Conn) {
	for {
		_, p, err := conn.ReadMessage()
		if err != nil {
			log.Println(err)
			return
		}
		publishRabbitMQ(p)
	}
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func publishRabbitMQ(message []byte) {
	conn, err := amqp.Dial("amqp://guest:guest@127.0.0.1:5672/")
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	err = ch.ExchangeDeclare(
		"public", // name
		"fanout", // type
		true,     // durable
		false,    // auto-deleted
		false,    // internal
		false,    // no-wait
		nil,      // arguments
	)
	failOnError(err, "Failed to declare an exchange")

	err = ch.Publish(
		"public", // exchange
		"",       // routing key
		false,    // mandatory
		false,    // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        message,
		})
	failOnError(err, "Failed to publish a message")

	log.Printf(" [x] Sent %s", message)
}

//https://www.calhoun.io/creating-random-strings-in-go/
func generateToken(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	token := string(b)
	if tokens[token] {
		token = generateToken(40)
	}
	return token
}

func wsEndPoint(c *gin.Context) {
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }

	ws, err := upgrader.Upgrade(c.Writer, c.Request, nil)
	failOnError(err, "Failed to upgrade connection to websocket")

	log.Println("Client Successfully Connected")

	reader(ws)
}

func homeEndPoint(c *gin.Context) {
	c.HTML(http.StatusOK, "index.html", nil)
}

func requestToken(c *gin.Context) {
	token := generateToken(40)
	tokens[token] = true
	c.JSON(200, gin.H{
		"token": token,
	})
}

func main() {
	fmt.Println("Go WebSockets")
	tokens = make(map[string]bool)

	r := gin.Default()

	r.LoadHTMLGlob("templates/*")
	r.Static("/static", "./static")
	// r.StaticFS("/more_static", http.Dir("my_file_system"))
	// r.StaticFile("/favicon.ico", "./resources/favicon.ico")

	r.GET("/", homeEndPoint)
	r.GET("/ws", wsEndPoint)
	r.POST("/request-token", requestToken)
	r.Use(cors.Default())
	r.Run(":8000")
}

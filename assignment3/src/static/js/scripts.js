function initConsummerWS() {
  let ws_stomp_display = new WebSocket("ws://127.0.0.1:15674/ws");
  let client_display = Stomp.over(ws_stomp_display);
  let mq_queue_display = "/exchange/public";

  let on_connect_display = () => {
    console.log("connected");
    client_display.subscribe(mq_queue_display, on_message_display);
  };

  let on_error_display = () => {
    console.log("error");
  };

  let on_message_display = (m) => {
    console.log("message received");
    appendChat(m.body);
  };

  client_display.connect(
    "guest",
    "guest",
    on_connect_display,
    on_error_display,
    "/"
  );
}

function initPublisherWS() {
  socket = new WebSocket("ws://127.0.0.1:8000/ws");
  console.log("Attempting Publisher Websocket Connection");

  socket.onclose = (event) => {
    console.log("Socket Closed Connection: ", event);
  };

  socket.onerror = (error) => {
    console.log("Socket Error: :", error);
  };
}

function publish() {
  let input = document.getElementById("chattext");
  if (input.value != ""){
    socket.send(token + " " + input.value);
    input.value = "";
  }
}

function appendChat(message) {
  let splittedMessage = message.split(" ");
  user = splittedMessage.shift();
  chat = splittedMessage.join(" ");
  let toBeAppended;
  if(user === token){
    toBeAppended ='<div class="d-flex flex-row p-3">'+
    '<img src="https://img.icons8.com/color/48/000000/circled-user-male-skin-type-7.png" width="30" height="30">'+
    '<div class="bg-white mr-2 p-3">'+ chat +
    '</div>'+
    '</div>';
  }
  else{
    toBeAppended ='<div class="d-flex flex-row p-3">'+
    '<img src="https://img.icons8.com/color/48/000000/circled-user-female-skin-type-7.png" width="30" height="30">'+
    '<div class="chat ml-2 p-3">'+ chat +
    '</div>'+
    '</div>'
  }
  $("#chats").append(toBeAppended);
}

function main() {
  axios.post("http://localhost:8000/request-token")
  .then(response => {
    token = response.data.token;
  });
  initConsummerWS();
  initPublisherWS();
}
main();

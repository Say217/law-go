module law/assignment3

go 1.16

require (
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.7.1
	github.com/gorilla/websocket v1.4.2
	github.com/streadway/amqp v1.0.0
)

module compress-service

go 1.16

require (
	cloud.google.com/go v0.67.0 // indirect
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.3.0
	golang.org/x/net v0.0.0-20200930145003-4acb6c075d10 // indirect
	golang.org/x/oauth2 v0.0.0-20200902213428-5d25da1a8d43
)

package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/joho/godotenv"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

var (
	googleOauthConfig *oauth2.Config
	randomState       string
	files             map[string]File
)

type File struct {
	Filepath     string `json:"filepath"`
	Header       string `json:"header"`
	Size         string // Go 1.9
	DateModified string `json:"dateModified"`
}

func init() {
	godotenv.Load(".env")
	googleOauthConfig = &oauth2.Config{
		RedirectURL:  "http://localhost:8001/auth/callback",
		ClientID:     os.Getenv("GOOGLE_CLIENT_ID"),
		ClientSecret: os.Getenv("GOOGLE_CLIENT_SECRET"),
		Scopes:       []string{"https://www.googleapis.com/auth/userinfo.email"},
		Endpoint:     google.Endpoint,
	}
	randomState = os.Getenv("random_string")
}

func gAuthCallback(w http.ResponseWriter, r *http.Request) {
	if r.FormValue("state") != randomState {
		fmt.Println("state is not valid")
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}
	token, err := googleOauthConfig.Exchange(context.TODO(), r.FormValue("code"))
	if err != nil {
		fmt.Printf("could not get token %s\n", err.Error())
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	response, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + token.AccessToken)

	if err != nil {
		fmt.Printf("could not get request: %s\n", err.Error())
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	defer response.Body.Close()
	contents, err := ioutil.ReadAll(response.Body)
	fmt.Fprintf(w, "Content: %s\n", contents)
	if err != nil {
		fmt.Printf("could not parse response: %s\n", err.Error())
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	w.Header().Set("Token", token.AccessToken)
	fmt.Fprintf(w, token.AccessToken)
	//json.NewEncoder(w).Encode(token.AccessToken)
}

func gAuthHandler(w http.ResponseWriter, r *http.Request) {
	url := googleOauthConfig.AuthCodeURL(randomState)
	http.Redirect(w, r, url, http.StatusTemporaryRedirect)
}

// func isAuthorized(string) {
// 	//http.Post("https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=accessToken")
// }

func main() {

	// Init mux Router
	router := mux.NewRouter()

	origins := handlers.AllowedOrigins([]string{"*"})

	files = make(map[string]File)

	// Route Handlers / Endpoints
	router.HandleFunc("/auth/callback", gAuthCallback).Methods("GET")
	router.HandleFunc("/auth", gAuthHandler).Methods("GET")
	router.HandleFunc("/create", createFile).Methods("POST")
	router.HandleFunc("/", handleHome).Methods("GET")
	router.HandleFunc("/read", readFile).Methods("GET")
	router.HandleFunc("/update", updateFile).Methods("PUT")
	router.HandleFunc("/delete", deleteFile).Methods("DELETE")

	log.Fatal(http.ListenAndServe(":8001", handlers.CORS(origins)(router)))
}

func handleHome(w http.ResponseWriter, r *http.Request) {
	var html = `<html><body><a href="/auth">Log in with Google</a></body></html>`
	fmt.Fprint(w, html)
}

func createFile(w http.ResponseWriter, r *http.Request) {
	filepath := r.FormValue("filepath")
	if val, ok := files[filepath]; ok {
		json.NewEncoder(w).Encode(val.Filepath + " not available")
	}
	header := r.FormValue("header")
	size := r.FormValue("size")
	dateModified := time.Now().String()
	file := File{Filepath: filepath, Header: header, Size: size, DateModified: dateModified}
	files[filepath] = file
	json.NewEncoder(w).Encode(files[filepath])
}

func readFile(w http.ResponseWriter, r *http.Request) {
	filepath := r.FormValue("filepath")

	if val, ok := files[filepath]; ok {
		json.NewEncoder(w).Encode(val)
	} else {
		json.NewEncoder(w).Encode("file not found")
	}
}

func updateFile(w http.ResponseWriter, r *http.Request) {
	filepath := r.FormValue("filepath")
	if val, ok := files[filepath]; ok {
		val.Header = r.FormValue("header")
		val.Size = r.FormValue("size")
		val.DateModified = time.Now().String()
		json.NewEncoder(w).Encode(val)
	} else {
		json.NewEncoder(w).Encode("file not found")
	}
}

func deleteFile(w http.ResponseWriter, r *http.Request) {
	filepath := r.FormValue("filepath")
	if val, ok := files[filepath]; ok {
		json.NewEncoder(w).Encode(val)
		delete(files, val.Filepath)
	} else {
		json.NewEncoder(w).Encode("file not found")
	}
}

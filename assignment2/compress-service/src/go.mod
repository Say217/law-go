module compress-service

go 1.16

require (
	cloud.google.com/go v0.79.0 // indirect
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.3.0
	golang.org/x/oauth2 v0.0.0-20210313182246-cd4f82c27b84
)

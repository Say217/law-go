package main

import (
	"archive/tar"
	"compress/gzip"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/joho/godotenv"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

var (
	googleOauthConfig *oauth2.Config
	randomState       string
)

func init() {
	godotenv.Load(".env")
	googleOauthConfig = &oauth2.Config{
		RedirectURL:  "http://localhost:8002/auth/callback",
		ClientID:     os.Getenv("GOOGLE_CLIENT_ID"),
		ClientSecret: os.Getenv("GOOGLE_CLIENT_SECRET"),
		Scopes:       []string{"https://www.googleapis.com/auth/userinfo.email"},
		Endpoint:     google.Endpoint,
	}
	randomState = os.Getenv("random_string")
}

func gAuthCallback(w http.ResponseWriter, r *http.Request) {
	if r.FormValue("state") != randomState {
		fmt.Println("state is not valid")
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}
	token, err := googleOauthConfig.Exchange(context.TODO(), r.FormValue("code"))
	if err != nil {
		fmt.Printf("could not get token %s\n", err.Error())
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	response, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + token.AccessToken)

	if err != nil {
		fmt.Printf("could not get request: %s\n", err.Error())
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	defer response.Body.Close()
	contents, err := ioutil.ReadAll(response.Body)
	fmt.Fprintf(w, "Content: %s\n", contents)
	if err != nil {
		fmt.Printf("could not parse response: %s\n", err.Error())
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	w.Header().Set("Token", token.AccessToken)
	json.NewEncoder(w).Encode(response.Body)
}

func gAuthHandler(w http.ResponseWriter, r *http.Request) {
	url := googleOauthConfig.AuthCodeURL(randomState)
	http.Redirect(w, r, url, http.StatusTemporaryRedirect)
}

func main() {

	// Init mux Router
	router := mux.NewRouter()

	origins := handlers.AllowedOrigins([]string{"*"})

	// Route Handlers / Endpoints
	router.HandleFunc("/auth/callback", gAuthCallback).Methods("GET")
	router.HandleFunc("/auth", gAuthHandler).Methods("GET")
	router.HandleFunc("/", handleHome).Methods("GET")
	router.HandleFunc("/compress", createTempFile).Methods("POST")
	router.HandleFunc("/download", downloadFile).Methods("GET")

	log.Fatal(http.ListenAndServe(":8002", handlers.CORS(origins)(router)))
}

func downloadFile(w http.ResponseWriter, r *http.Request) {
	fmt.Print(r.FormValue("filepath"))
	http.ServeFile(w, r, r.FormValue("filepath"))
}

func handleHome(w http.ResponseWriter, r *http.Request) {
	var html = `<html><body><a href="/auth">Log in with Google</a></body></html>`
	fmt.Fprint(w, html)
}

func createTempFile(w http.ResponseWriter, r *http.Request) {
	r.ParseMultipartForm(10 << 20)

	file, handler, err := r.FormFile("myFile")
	if err != nil {
		fmt.Println("Error Retrieving file from form-data")
		fmt.Println(err)
		return
	}

	defer file.Close()

	tempFile, err := ioutil.TempFile("temp-files", "*"+handler.Filename)

	if err != nil {
		fmt.Println(err)
	}
	defer tempFile.Close()

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
	}
	tempFile.Write(fileBytes)

	// set up the output file
	fileOut, err := os.Create(tempFile.Name() + ".tar.gz")
	if err != nil {
		log.Fatalln(err)
	}
	defer file.Close()
	// set up the gzip writer
	gw := gzip.NewWriter(fileOut)
	defer gw.Close()
	tw := tar.NewWriter(gw)
	defer tw.Close()

	addFile(tw, tempFile.Name())
	w.Header().Set("Content-type", "application/x-gzip")
	json.NewEncoder(w).Encode(tempFile.Name() + ".tar.gz")
}

func addFile(tw *tar.Writer, path string) error {
	file, err := os.Open(path)
	if err != nil {
		return err
	}
	defer file.Close()
	if stat, err := file.Stat(); err == nil {
		// now lets create the header as needed for this file within the tarball
		header := new(tar.Header)
		header.Name = path
		header.Size = stat.Size()
		header.Mode = int64(stat.Mode())
		header.ModTime = stat.ModTime()
		// write the header to the tarball archive
		if err := tw.WriteHeader(header); err != nil {
			return err
		}
		// copy the file data to the tarball
		if _, err := io.Copy(tw, file); err != nil {
			return err
		}
	}
	return nil
}
